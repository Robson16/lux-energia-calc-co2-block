import { useEffect, useState } from '@wordpress/element';
import { __, sprintf } from '@wordpress/i18n';
import { Container, Column, Label, Valor } from './styles';
import CurrencyFormat from 'react-currency-format';

const Display = ( {
	colors,
	currencySymbol,
	monthlyValue,
	averageAnnualFactor,
	unitCost,
	...rest
} ) => {
	const [ monthlySavings, setMonthlySavings ] = useState( 0 );
	const [ annualSavings, setAnnualSavings ] = useState( 0 );
	const [ avoidedEmissions, setAvoidedEmissions ] = useState( 0 );

	useEffect( () => {
		setMonthlySavings( monthlyValue * 0.4 );
		setAnnualSavings( monthlySavings * 12 );
		setAvoidedEmissions(
			( monthlyValue / unitCost ) * averageAnnualFactor
		);
	}, [ monthlyValue, monthlySavings, annualSavings, avoidedEmissions ] );

	return (
		<Container { ...rest }>
			<Column>
				<Label color={ colors.label }>
					{ sprintf(
						// translators: %s: Currency Symbol
						__(
							'Monthly Invoice Amount (%s)',
							'luxenergia-co2calc'
						),
						currencySymbol
					) }
				</Label>
				<CurrencyFormat
					value={ monthlyValue }
					displayType={ 'text' }
					thousandSeparator={ '.' }
					decimalSeparator={ ',' }
					decimalScale={ 2 }
					fixedDecimalScale
					prefix={ `${ currencySymbol } ` }
					renderText={ ( value ) => (
						<Valor color={ colors.value }>{ value }</Valor>
					) }
				/>
			</Column>
			<Column>
				<Label color={ colors.label }>
					{ sprintf(
						// translators: %s: Currency Symbol
						__(
							'Estimated monthly savings (%s)',
							'luxenergia-co2calc'
						),
						currencySymbol
					) }
				</Label>
				<CurrencyFormat
					value={ monthlySavings }
					displayType={ 'text' }
					thousandSeparator={ '.' }
					decimalSeparator={ ',' }
					decimalScale={ 2 }
					fixedDecimalScale
					prefix={ `${ currencySymbol } ` }
					renderText={ ( value ) => (
						<Valor color={ colors.value }>{ value }</Valor>
					) }
				/>
			</Column>
			<Column>
				<Label color={ colors.label }>
					{ sprintf(
						// translators: %s: Currency Symbol
						__(
							'Estimated annual savings (%s)',
							'luxenergia-co2calc'
						),
						currencySymbol
					) }
				</Label>
				<CurrencyFormat
					value={ annualSavings }
					displayType={ 'text' }
					thousandSeparator={ '.' }
					decimalSeparator={ ',' }
					decimalScale={ 2 }
					fixedDecimalScale
					prefix={ `${ currencySymbol } ` }
					renderText={ ( value ) => (
						<Valor color={ colors.value }>{ value }</Valor>
					) }
				/>
			</Column>
			<Column>
				<Label color={ colors.label }>
					{ __(
						'Tons of avoided emissions (tCO²/month)',
						'luxenergia-co2calc'
					) }
				</Label>
				<CurrencyFormat
					value={ avoidedEmissions }
					displayType={ 'text' }
					thousandSeparator={ '.' }
					decimalSeparator={ ',' }
					decimalScale={ 2 }
					fixedDecimalScale
					suffix={ ` tCO²` }
					renderText={ ( value ) => (
						<Valor color={ colors.value }>{ value }</Valor>
					) }
				/>
			</Column>
		</Container>
	);
};

export default Display;
