��          �       |      |  ,   }     �     �     �     �     �          ;     W  
   \  
   g     r     �     �     �  &   �     �  	   �     �  6   �  8     �  H  ;        K     g     m          �  &   �     �     �                     4     D     J  +   Q     }     �     �  6   �  8   �   A plugin to add a interactive CO2 Calculator Average Annual Factor (5 years) Colors Currency Symbol Estimated annual savings (%s) Estimated monthly savings (%s) Lux Energia - CO2 Calc Block Monthly Invoice Amount (%s) Rail Range Max. Range Min. Robson H. Rodrigues Settings Text Thumb Tons of avoided emissions (tCO2/month) Track Unit cost Values https://gitlab.com/Robson16/lux-energia-co2-calc-block https://www.linkedin.com/in/robson-h-rodrigues-93341746/ Project-Id-Version: Lux Energia - CO2 Calc Block
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2023-06-21 21:08+0000
PO-Revision-Date: 2023-06-21 22:14+0000
Last-Translator: 
Language-Team: Português do Brasil
Language: pt_BR
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.6.4; wp-6.2.2
X-Domain: luxenergia-co2calc Um plug-in para adicionar uma calculadora de CO2 interativa Fator Médio Anual (5 anos) Cores Símbolo da Moeda Economia anual estimada (%s) Economia mensal estimada (%s) Lux Energia - Bloco de Cálculo de CO2 Valor da fatura mensal (%s) Trilho Alcance máx. Alcance mín. Robson H. Rodrigues Configurações Texto Dedão Toneladas de emissões evitadas (tCO2/mês) Rastro Custo unitário Valores https://gitlab.com/Robson16/lux-energia-co2-calc-block https://www.linkedin.com/in/robson-h-rodrigues-93341746/ 