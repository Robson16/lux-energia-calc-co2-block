import domReady from '@wordpress/dom-ready';
import { Fragment, render, useState } from '@wordpress/element';
import Display from './components/Display';
import Slider from './components/Slider';

const Co2Calc = ( { attributes } ) => {
	const {
		thumbColor,
		trackColor,
		railColor,
		labelColor,
		valueColor,
		rangeMin,
		rangeMax,
		currencySymbol,
		averageAnnualFactor,
		unitCost,
	} = attributes;

	const [ monthlyValue, setMonthlyValue ] = useState( rangeMin );

	return (
		<Fragment>
			<Slider
				defaultValue={ monthlyValue }
				trackColor={ trackColor }
				railColor={ railColor }
				thumbColor={ thumbColor }
				min={ rangeMin }
				max={ rangeMax }
				value={ monthlyValue }
				onChange={ ( value ) => {
					setMonthlyValue( value );
				} }
			/>
			<Display
				colors={ { label: labelColor, value: valueColor } }
				currencySymbol={ currencySymbol }
				monthlyValue={ monthlyValue }
				averageAnnualFactor={ averageAnnualFactor }
				unitCost={ unitCost }
			/>
		</Fragment>
	);
};

domReady( function () {
	const co2calc = document.querySelector( '#co2calc' );
	const attributes = JSON.parse( co2calc.dataset.attributes );

	render( <Co2Calc attributes={ attributes } />, co2calc );
} );
