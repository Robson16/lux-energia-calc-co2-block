import ReactSlider from 'react-slider';
import styled from 'styled-components';

export const StyledSlider = styled( ReactSlider )`
	width: 100%;
	height: 12px;
`;

export const StyledTrack = styled.div`
	top: 0;
	bottom: 0;
	background: ${ ( { index, railColor, trackColor } ) =>
		index === 1 ? railColor : trackColor };
	border-radius: 999px;
`;

export const StyledThumb = styled.div`
	margin-top: ${ ( 45 / 2 - 12 / 2 ) * -1 }px;
	height: 45px;
	line-height: 45px;
	width: 45px;
	text-align: center;
	background-color: ${ ( { thumbColor } ) => thumbColor };
	color: ${ ( { thumbColor } ) => thumbColor };
	border-radius: 50%;
	cursor: grab;
	outline: none;
`;
