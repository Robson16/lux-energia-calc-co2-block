import { StyledSlider, StyledTrack, StyledThumb } from './styles';

const Slider = ( { trackColor, railColor, thumbColor, ...rest } ) => {
	const Thumb = ( props, state ) => (
		<StyledThumb thumbColor={ thumbColor } { ...props } />
	);

	const Track = ( props, state ) => (
		<StyledTrack
			trackColor={ trackColor }
			railColor={ railColor }
			{ ...props }
			index={ state.index }
		/>
	);

	return (
		<StyledSlider
			{ ...rest }
			snapDragDisabled
			renderTrack={ Track }
			renderThumb={ Thumb }
		/>
	);
};

export default Slider;
