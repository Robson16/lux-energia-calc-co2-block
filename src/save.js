import { useBlockProps } from '@wordpress/block-editor';

export default function save( { attributes } ) {
	return (
		<div
			{ ...useBlockProps.save( {
				id: 'co2calc',
				'data-attributes': JSON.stringify( attributes ),
			} ) }
		/>
	);
}
