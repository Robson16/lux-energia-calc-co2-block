import styled from 'styled-components';

export const Container = styled.div`
	display: flex;
	margin-top: 3rem;

	@media ( max-width: 767.98px ) {
		flex-direction: column;
	}
`;

export const Column = styled.div`
	display: flex;
	flex-direction: column;

	@media ( max-width: 767.98px ) {
		&:not( :first-of-type ) {
			margin-top: 2rem;
		}
	}

	@media ( min-width: 768px ) {
		padding-right: 2rem;
		padding-left: 2rem;

		&:not( :last-of-type ) {
			border-right: 1px solid #ffffff;
		}

		&:first-of-type {
			padding-left: 0;
		}

		&:last-of-type {
			padding-right: 0;
		}
	}
`;

export const Label = styled.span`
	font-size: 1.3rem;
	color: ${ ( { color } ) => color };

	@media ( min-width: 768px ) {
		margin-bottom: 2rem;
	}
`;

export const Valor = styled.strong`
	font-size: 1.49rem;
	color: ${ ( { color } ) => color };
`;
