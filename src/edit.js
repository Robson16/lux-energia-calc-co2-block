import { useBlockProps } from '@wordpress/block-editor';
import { InspectorControls } from '@wordpress/blockEditor';
import {
	ColorPalette,
	PanelBody,
	PanelRow,
	TextControl,
} from '@wordpress/components';
import { Fragment, useState, useEffect } from '@wordpress/element';
import { __ } from '@wordpress/i18n';
import Display from './components/Display';
import Slider from './components/Slider';

export default function Edit( { attributes, setAttributes } ) {
	const {
		thumbColor,
		trackColor,
		railColor,
		labelColor,
		valueColor,
		rangeMin,
		rangeMax,
		currencySymbol,
		averageAnnualFactor,
		unitCost,
	} = attributes;

	const [ monthlyValue, setMonthlyValue ] = useState( rangeMin );

	useEffect( () => {
		setMonthlyValue( rangeMin );
	}, [ rangeMin ] );

	return (
		<Fragment>
			<InspectorControls>
				<PanelBody
					title={ __( 'Colors', 'luxenergia-co2calc' ) }
					initialOpen={ false }
				>
					<PanelRow>
						<h2>{ __( 'Thumb', 'luxenergia-co2calc' ) }</h2>
						<ColorPalette
							enableAlpha
							clearable={ false }
							value={ thumbColor }
							onChange={ ( color ) =>
								setAttributes( {
									thumbColor: color,
								} )
							}
						/>
					</PanelRow>
					<PanelRow>
						<h2>{ __( 'Track', 'luxenergia-co2calc' ) }</h2>
						<ColorPalette
							enableAlpha
							clearable={ false }
							value={ trackColor }
							onChange={ ( color ) =>
								setAttributes( {
									trackColor: color,
								} )
							}
						/>
					</PanelRow>
					<PanelRow>
						<h2>{ __( 'Rail', 'luxenergia-co2calc' ) }</h2>
						<ColorPalette
							enableAlpha
							clearable={ false }
							value={ railColor }
							onChange={ ( color ) =>
								setAttributes( {
									railColor: color,
								} )
							}
						/>
					</PanelRow>
					<PanelRow>
						<h2>{ __( 'Text', 'luxenergia-co2calc' ) }</h2>
						<ColorPalette
							enableAlpha
							clearable={ false }
							value={ labelColor }
							onChange={ ( color ) =>
								setAttributes( {
									labelColor: color,
								} )
							}
						/>
					</PanelRow>
					<PanelRow>
						<h2>{ __( 'Values', 'luxenergia-co2calc' ) }</h2>
						<ColorPalette
							enableAlpha
							clearable={ false }
							value={ valueColor }
							onChange={ ( color ) =>
								setAttributes( {
									valueColor: color,
								} )
							}
						/>
					</PanelRow>
				</PanelBody>
			</InspectorControls>

			<InspectorControls>
				<PanelBody
					title={ __( 'Settings', 'luxenergia-co2calc' ) }
					initialOpen={ false }
				>
					<PanelRow>
						<TextControl
							label={ __( 'Range Min.', 'luxenergia-co2calc' ) }
							type="number"
							value={ rangeMin }
							onChange={ ( value ) =>
								setAttributes( {
									rangeMin: Number( value ),
								} )
							}
						/>
					</PanelRow>
					<PanelRow>
						<TextControl
							label={ __( 'Range Max.', 'luxenergia-co2calc' ) }
							type="number"
							value={ rangeMax }
							onChange={ ( value ) =>
								setAttributes( {
									rangeMax: Number( value ),
								} )
							}
						/>
					</PanelRow>
					<PanelRow>
						<TextControl
							label={ __(
								'Currency Symbol',
								'luxenergia-co2calc'
							) }
							value={ currencySymbol }
							onChange={ ( value ) =>
								setAttributes( { currencySymbol: value } )
							}
						/>
					</PanelRow>
					<PanelRow>
						<TextControl
							label={ __(
								'Average Annual Factor (5 years)',
								'luxenergia-co2calc'
							) }
							type="number"
							value={ averageAnnualFactor }
							onChange={ ( value ) =>
								setAttributes( {
									averageAnnualFactor: value,
								} )
							}
						/>
					</PanelRow>
					<PanelRow>
						<TextControl
							label={ __( 'Unit cost', 'luxenergia-co2calc' ) }
							type="number"
							value={ unitCost }
							onChange={ ( value ) =>
								setAttributes( {
									unitCost: value,
								} )
							}
						/>
					</PanelRow>
				</PanelBody>
			</InspectorControls>

			<div { ...useBlockProps() }>
				<Slider
					defaultValue={ monthlyValue }
					trackColor={ trackColor }
					railColor={ railColor }
					thumbColor={ thumbColor }
					min={ rangeMin }
					max={ rangeMax }
					value={ monthlyValue }
					onChange={ ( value ) => {
						setMonthlyValue( value );
					} }
				/>
				<Display
					colors={ { label: labelColor, value: valueColor } }
					currencySymbol={ currencySymbol }
					monthlyValue={ monthlyValue }
					averageAnnualFactor={ averageAnnualFactor }
					unitCost={ unitCost }
				/>
			</div>
		</Fragment>
	);
}
