<?php

/**
 * Plugin Name:       Lux Energia - CO2 Calc Block
 * Plugin URI:        https://gitlab.com/Robson16/lux-energia-co2-calc-block
 * Description:       A plugin to add a interactive CO2 Calculator
 * Requires at least: 6.1
 * Requires PHP:      7.0
 * Version:           1.0.0
 * Author:            Robson H. Rodrigues
 * Author URI:        https://www.linkedin.com/in/robson-h-rodrigues-93341746/
 * License:           MIT
 * Text Domain:       luxenergia-co2calc
 * Domain Path:       /languages
 *
 * @package           create-block
 */

function luxenergia_calcco2_block_init()
{
	register_block_type(__DIR__ . '/build');

	wp_set_script_translations(
		'luxenergia-co2calc-editor-script',
		'luxenergia-co2calc',
		plugin_dir_path(__FILE__) . 'languages'
	);

	wp_set_script_translations(
		'luxenergia-co2calc-view-script',
		'luxenergia-co2calc',
		plugin_dir_path(__FILE__) . 'languages'
	);
}
add_action('init', 'luxenergia_calcco2_block_init');
